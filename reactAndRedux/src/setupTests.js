import React from 'react'
import { MemoryRouter } from 'react-router'
import '@testing-library/jest-dom/extend-expect'
import {
    configure, shallow, mount,
} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import { store } from './store'
import { AppContainer } from './App'
import List from './Components/List'
import MainPage from './MainSection/MainPage'
import SectionForm from './Components/SectionForm'
import FormComment from './CommentsSection/AddCommentSection/FormComment'
import AddComment from './CommentsSection/AddCommentSection/AddComment'
import Filter from './FilterSection/Filter'
import SelectedComments from './CommentsSection/SelectedComments'
import Nav from './Navigation/Nav'
import NavPart from './Navigation/NavPart'
import FormButtons from './Components/FormButtons'
import Input from './Components/Input'
import Title from './Components/Title'
import SingleComment from './Components/SingleComment'
import { selectComment } from './actions'

configure({adapter: new Adapter()})

it('renders without crashing', () => {
    shallow(<Provider store={store}>
        <AppContainer/>
    </Provider>)
})

it('includes Sections in App', () => {
    const app = shallow(<Provider store={store}>
        <AppContainer/>
    </Provider>)
    expect(app.find('.container')).toBeDefined()
    const wrapper = mount
    (<Provider store={store}>
        <AppContainer/>
    </Provider>)
    expect(wrapper.find(MainPage)).toHaveLength(1)
    expect(wrapper.find(SelectedComments)).toHaveLength(1)
    expect(wrapper.find(Filter)).toHaveLength(1)

})
it('includes Nav in App', () => {
    const app = mount(<Provider store={store}>
        <AppContainer/>
    </Provider>)
    expect(app.find(Nav)).toHaveLength(1)
})
it('renders MainPage  without crashing', () => {
    shallow(<MainPage/>)
})
it('renders  Filter without crashing', () => {
    shallow(<Filter/>)
})
it('renders  AddComment without crashing', () => {
    shallow(<AddComment/>)
})
it('renders  Nav without crashing', () => {
    shallow(<Nav/>)
})
it('renders elements in  Nav', () => {
    const app = mount(<Nav/>)
    expect(app.find('a')).toHaveLength(4)
})

it('renders MainPage without crashing', () => {
    shallow(<MainPage/>)
})
it('renders  AddComment without crashing', () => {
    shallow(<AddComment/>)
})
it('renders FormComment without crashing', () => {
    shallow(<FormComment/>)
})
it('state FormComment', () => {
    const app = shallow(<FormComment/>)
    expect(app.state('name')).toBe('')
    expect(app.state('email')).toBe('')
    expect(app.state('body')).toBe('')
    expect(app.state('showError')).toBe(null)
})
it('functions in FormComment', () => {
    const app = mount(<FormComment/>)
    app.instance().setValue('name', 'lelum polelum')
    app.instance().setValue('email', 'lelum@polelum.com')
    app.instance().setValue('body', 'lelum polelum')
    app.instance().validate('name', 'lelum polelum')
    app.instance().validate('email', 'lelum@polelum.com')
    app.instance().validate('body', 'lelum polelum')
    app.instance().clearState()
})
it('FormButtons in FormComment', () => {
    const app = shallow(<FormComment/>)
    expect(app.containsMatchingElement(<FormButtons/>)).toEqual(true)
    expect(app.find('#form-comment')).toBeDefined()

})
it('SectionForm in FormComment', () => {
    const app = shallow(<FormComment/>)
    expect(app.containsMatchingElement(<FormButtons/>)).toEqual(true)
})
it('renders SectionForm without crashing', () => {
    shallow(<SectionForm/>)
})
it('input in Section', () => {
    const app = shallow(<SectionForm/>)
    expect(app.containsMatchingElement(<Input/>)).toEqual(true)
})

it('renders SelectedComments without crashing', () => {
    shallow(<SelectedComments/>)
})
it('renders SingleComment without crashing', () => {
    const comment = {name: 'name', body: 'body', email: 'a@wp.pl'}
    shallow(<SingleComment comment={comment}/>)
})
it('renders elements in  SingleComment', () => {
    const comment = {name: 'name', body: 'body', email: 'a@wp.pl'}
    const app = mount(<SingleComment comment={comment}/>)
    expect(app.find('div')).toHaveLength(3)
    expect(app.find('span')).toHaveLength(6)
})
it('renders Nav without crashing', () => {
    shallow(<Nav/>)
})
it('renders NavPart without crashing', () => {
    shallow(<NavPart/>)
})
it('renders elements in  NavPart', () => {
    const app = mount(<NavPart/>)
    expect(app.find('a')).toHaveLength(1)
})
it('includes elements in Filter', () => {
    const app = shallow(<Filter/>)
    expect(app.containsMatchingElement(<FormButtons/>)).toEqual(true)
    expect(app.find('option')).toHaveLength(3)
    expect(app.find('.select-section')).toBeDefined()
    expect(app.find('#form-comment')).toBeDefined()
})
