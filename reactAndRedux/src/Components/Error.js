import React from 'react'
import '../style/components.css'
import PropTypes from 'prop-types'
import { BrowserRouter as Router } from 'react-router-dom'

const Error = (props) => (
    <Router>
        <div className="error-msg">{props.msg}</div>
    </Router>
)

export default Error

Error.propTypes = {
    msg: PropTypes.string,
}

Error.defaultProps = {
    title: 'Error - check the data you entered',
}