import React from 'react'
import '../style/components.css'
import { Route } from 'react-router-dom'
import PropTypes from 'prop-types'
import Button from './Button'
import SingleComment from './SingleComment'
import { useDispatch } from 'react-redux'
import { refresh } from '../Utils/UtilsFunctions'

export const List = (props) => {
    const dispatch = useDispatch()
    return (
        <div className="grid-container">
            {props.comments.map((item, index) => (
                <div className="comment" key={index}>
                    <Route path="/"
                           component={() => <SingleComment comment={item}/>}/>
                    {props.addSelectedComments !== undefined && (
                        <Route
                            path="/"
                            component={() => (<Button buttonClass="add-btn"
                                                      onClick={() => dispatch({
                                                          type: 'SELECT',
                                                          comment: item,
                                                      })}
                                                      title="Add to selected comments"/>)}/>
                    )}
                    {props.deleteComment !== undefined && (
                        <Route
                            path="/"
                            component={() => (<Button buttonClass="add-btn"
                                                      onClick={() => {
                                                          dispatch({
                                                              type: 'DELETE',
                                                              comment: item,
                                                          })
                                                          refresh()
                                                      }}
                                                      title="Delete comment"/>)}/>
                    )}
                </div>
            ))}
        </div>
    )
}

export default List

List.propTypes = {
    comments: PropTypes.array,
}

List.defaultProps = {
    comments: [],
}