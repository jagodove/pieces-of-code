import React from 'react'
import '../style/components.css'
import PropTypes from 'prop-types'
import { BrowserRouter as Router } from 'react-router-dom'

const SingleComment = (props) => (
    <Router>
        <React.Fragment>
            <span className="title-info">Nazwa</span>
            <span className="info"> {props.comment.name}</span>
        </React.Fragment>
        <div>
            <span className="title-info"> Email</span>
            <span className="info">{props.comment.email}</span>
        </div>
        <div>
            <span className="title-info">Comment </span>
            <span className="info">{props.comment.body.slice(0, 20)}</span>
        </div>
    </Router>
)

export default SingleComment

SingleComment.propTypes = {
    comment: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        email: PropTypes.string,
        body: PropTypes.string,
    }),
}

