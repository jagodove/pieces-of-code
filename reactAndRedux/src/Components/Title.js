import React from 'react'
import '../style/components.css'
import PropTypes from 'prop-types'
import { BrowserRouter as Router } from 'react-router-dom'

const Title = (props) => (
    <Router>
        <h2> {props.title} </h2>
    </Router>
)

export default Title

Title.propTypes = {
    title: PropTypes.string,
}
