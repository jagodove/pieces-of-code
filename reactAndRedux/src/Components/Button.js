import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import PropTypes from 'prop-types';
import "../style/components.css";


const Button = (props) => (
    <Router>
        <React.Fragment>
            <button
                type="button"
                className={props.buttonClass}
                onClick={props.onClick}>
                {props.title}
            </button>
        </React.Fragment>
    </Router>
);

export default Button;

Button.propTypes = {
    title : PropTypes.string
};

Button.defaultProps = {
    title: 'Click'
};
