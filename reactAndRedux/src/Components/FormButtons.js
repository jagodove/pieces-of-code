import React from 'react'
import '../style/components.css'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import PropTypes from 'prop-types'
import Button from './Button'

const FormButtons = (props) => (
    <Router>
        <div className="form-buttons">
            <Route
                path="/"
                component={() => (<Button buttonClass={props.buttonClass}
                                          onClick={props.open}
                                          title={props.title[0]}/>)}/>
            <Route
                path="/"
                component={() => (<Button buttonClass={props.buttonClass}
                                          onClick={(e) => props.submit(e)}
                                          title={props.title[1]}/>)}/>
        </div>
    </Router>
)

export default FormButtons

FormButtons.propTypes = {
    title: PropTypes.array,
    buttonClass: PropTypes.string,
    onClick: PropTypes.func,
}

FormButtons.defaultProps = {
    title: [['Back'], ['Add']],
    buttonClass: 'btn',
}