import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import PropTypes from 'prop-types'
import '../style/sections.css'
import Input from '../Components/Input'

class SectionForm extends React.Component {
    render () {
        return (
            <Router>
                <div className="section-form">
                    <span className="title-form">{this.props.title} </span>
                    <span>
                        <Input
                            type={this.props.type}
                            value={this.props.value}
                            valueName={this.props.valueName}
                            onChange={this.props.onChange}
                            onKeyPress={this.props.onKeyPress}/>
                     </span>
                </div>
            </Router>
        )
    }
}

export default SectionForm

SectionForm.propTypes = {
    title: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
    valueName: PropTypes.string,
}

SectionForm.defaultProps = {
    title: ' ',
}