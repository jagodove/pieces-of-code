import React from 'react'
import '../style/components.css'
import PropTypes from 'prop-types'
import { BrowserRouter as Router } from 'react-router-dom'

const Input = (props) => (
    <Router>
        <React.Fragment>
            <input
                type={props.type}
                value={props.value}
                onChange={(e) => props.onChange(props.valueName,
                    e.target.value)}
                onKeyPress={(e) => props.onKeyPress(props.valueName, e)}/>
        </React.Fragment>
    </Router>
)

export default Input

Input.propTypes = {
    type: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    onKeyPress: PropTypes.func,
}

Input.defaultProps = {
    type: 'text',
    value: '',
}
