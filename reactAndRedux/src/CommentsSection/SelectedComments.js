import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import PropTypes from 'prop-types'
import '../style/sections.css'
import List from '../Components/List'
import Title from '../Components/Title'
import Button from '../Components/Button'
import { openHomepage } from '../Utils/UtilsFunctions'

class SelectedComments extends React.Component {
    render () {
        return (
            <Router>
                <section id="second">
                    <div className="list">
                        <Route
                            path="/"
                            component={() => (<Button buttonClass="comment-btn"
                                                      onClick={openHomepage}
                                                      title="Back to homepage"/>)}/>
                        <Route
                            path="/"
                            component={() => <Title
                                title={'SELECTED COMMENTS'}/>}/>
                        <Route
                            path="/"
                            component={() => (
                                <List comments={this.props.comments}
                                      deleteComment={true}/>)}/>
                    </div>
                </section>
            </Router>
        )
    }
}

export default SelectedComments

SelectedComments.propTypes = {
    comments: PropTypes.array,
}