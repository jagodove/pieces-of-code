import React from 'react'
import '../../style/sections.css'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import FormComment from './FormComment'
import Title from '../../Components/Title'

class AddComment extends React.Component {
    render () {
        return (
            <Router>
                <section id="four">
                    <div className="form-comment">
                        <Route
                            path="/"
                            component={() => <Title
                                title={'Add new comment'}/>}/>
                        <Route
                            path="/"
                            component={() => <FormComment
                                onSubmit={this.props.onSubmit}/>}/>
                    </div>
                </section>
            </Router>
        )
    }
}

export default AddComment
