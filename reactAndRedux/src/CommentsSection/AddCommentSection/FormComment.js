import React from 'react'
import '../../style/sections.css'
import SectionForm from './../../Components/SectionForm'
import FormButtons from '../../Components/FormButtons'
import Error from '../../Components/Error'
import { openHomepage } from '../../Utils/UtilsFunctions'

class FormComment extends React.Component {
    state = {
        name: '',
        email: '',
        body: '',
        showError: null,
    }

    validate = (opt, e) => {
        let re
        let errorMsg

        opt === 'name' ? (re = /[a-fA-F]+/g) : (re = /^[0-9a-zA-Z]+$/)

        if (!re.test(e.key)) {
            e.preventDefault()
            opt === 'name'
                ? (errorMsg = 'The name may contain only letters')
                : (errorMsg = 'The comment may be only  alphanumeric characters')
            this.setState({showError: errorMsg})
        } else {
            this.setState({showError: null})
        }
    }

    setValue = (opt, value) => {
        this.setState({[opt]: value})
    }

    handleSubmit (e) {
        const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

        if (
            this.state.name === '' ||
            this.state.body === '' ||
            this.state.email === ''
        ) {
            this.setState({showError: 'Please complete the form'})
        } else if (this.state.name.length < 3) {
            this.setState({
                showError: 'The name must be at least 3 characters long',
            })
        } else if (this.state.body.length < 2) {
            this.setState({
                showError: 'The comment must be at least 2 characters long',
            })
        } else if (!re.test(this.state.email)) {
            this.setState({showError: 'Email is wrong'})
        } else if (
            this.state.name.length >= 3 &&
            this.state.body.length >= 2 &&
            re.test(this.state.email)
        ) {
            const item = {
                name: this.state.name,
                body: this.state.body,
                email: this.state.email,
            }
            this.props.onSubmit(item)
            openHomepage()
            this.setState(
                {name: ' ', email: ' ', body: ' ', showError: null},
                () => {},
            )
        }
    }

    clearState = () => {
        this.setState({name: ' ', email: ' ', body: ' ', showError: null},
            () => {
                openHomepage()
            })
    }

    render () {
        return (
            <React.Fragment>
                <SectionForm
                    title={'Name'}
                    type={'text'}
                    value={this.state.name}
                    valueName={'name'}
                    onChange={this.setValue.bind(this)}
                    onKeyPress={this.validate.bind(this)}/>
                <SectionForm
                    title={'Email'}
                    type={'email'}
                    value={this.state.email}
                    valueName={'email'}
                    onChange={this.setValue.bind(this)}
                    onKeyPress={() => {}}/>
                <SectionForm
                    title={'Comment'}
                    type={'text'}
                    value={this.state.body}
                    valueName={'body'}
                    onChange={this.setValue.bind(this)}
                    onKeyPress={this.validate.bind(this)}/>
                {this.state.showError != null &&
                <Error msg={this.state.showError}/>}
                <FormButtons
                    buttonClass="comment-btn"
                    open={this.clearState}
                    submit={this.handleSubmit.bind(this)}
                    onChange={this.setValue.bind(this)}
                    onKeyPress={this.validate.bind(this)}
                    title={['Back to homepage', 'Add comment']}/>
            </React.Fragment>
        )
    }
}

export default FormComment
