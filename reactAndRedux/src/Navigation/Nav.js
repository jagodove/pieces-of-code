import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { BsHouse, BsFunnel } from 'react-icons/bs'
import { FiEdit3 } from 'react-icons/fi'
import { GrUnorderedList } from 'react-icons/gr'
import NavPart from './NavPart'
import '../style/nav.css'

class Nav extends React.Component {
    render () {
        return (
            <Router>
                <React.Fragment>
                    <nav>
                        <ul>
                            <li>
                                <Route
                                    path="/"
                                    component={() => (<NavPart id={'mainpage'}
                                                               number={'#first'}
                                                               title="Main Page"
                                                               img={
                                                                   <BsHouse/>}/>)}/>
                            </li>
                            <li>
                                <Route
                                    path="/"
                                    component={() => (<NavPart id={'selected'}
                                                               number={'#second'}
                                                               title="Selected Comments"
                                                               img={
                                                                   <GrUnorderedList/>}/>)}/>
                                <ul>
                                    <li>
                                        <Route
                                            path="/"
                                            component={() => (
                                                <NavPart id={'newcomment'}
                                                         number={'#four'}
                                                         title="Add new comment"
                                                         img={<FiEdit3/>}/>)}/>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Route
                                    path="/"
                                    component={() => (<NavPart id={'filter'}
                                                               number={'#thrid'}
                                                               title="Filter"
                                                               img={
                                                                   <BsFunnel/>}/>)}/>
                            </li>
                        </ul>
                    </nav>
                </React.Fragment>
            </Router>
        )
    }
}

export default Nav
