import React from 'react'
import PropTypes from 'prop-types'
import '../style/nav.css'
import { BrowserRouter as Router } from 'react-router-dom'

const NavPart = (props) => (
    <Router>
        <a id={props.id} href={props.number}>
            {props.img}
            {props.title}
        </a>
    </Router>
)

export default NavPart

NavPart.propTypes = {
    id: PropTypes.string,
    number: PropTypes.string,
    img: PropTypes.object,
    title: PropTypes.string,
}
