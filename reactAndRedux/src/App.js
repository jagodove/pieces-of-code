import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './style/basic.css'
import Nav from './Navigation/Nav'
import MainPage from './MainSection/MainPage'
import SelectedComments from './CommentsSection/SelectedComments'
import Filter from './FilterSection/Filter'
import AddComment from './CommentsSection/AddCommentSection/AddComment'
import { openHomepage } from './Utils/UtilsFunctions'
import { connect } from 'react-redux'
import {
    commentsFetched,
    selectComment,
    deleteComment,
    addComment,
} from './actions'

class App extends React.Component {
    componentDidMount () {
        this.getComments()
    }

    getComments () {
        fetch('https://jsonplaceholder.typicode.com/comments/?_limit=20').
            then((res) => res.json()).
            then((resp) => {
                this.props.commentsFetched(resp)
                openHomepage()
            }).
            catch((error) => {
                console.error('Error', error)
                alert('Error downloading comments ')
            })
    }

    render () {
        return (
            <Router>
                <div className="container">
                    <Route path="/" component={Nav}/>
                    <Route
                        path="/"
                        component={() => <MainPage
                            comments={this.props.comments}/>}/>
                    <Route
                        path="/"
                        component={() => (<SelectedComments
                            comments={this.props.selectComments}/>)}/>
                    <Route
                        path="/"
                        component={() => <AddComment
                            onSubmit={this.props.addComment}/>}/>
                    <Route path="/" component={Filter}/>
                </div>
            </Router>
        )
    }
}

const mapStateToProps = (state) => ({
    comments: state.comments,
    selectComments: state.selectComments,
})

const mapDispatchToProps = {
    commentsFetched,
    selectComment,
    deleteComment,
    addComment,
}

export const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App)
