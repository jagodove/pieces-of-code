import { combineReducers } from 'redux'
import { comments, select } from './comments'

export default combineReducers({comments: comments, selectComments: select})
