const comments = (state = [], action) => {
    switch (action.type) {
        case 'FETCH_COMMENTS_SUCCESS':
            return [...action.comments]
        case 'ADD':
            const newComment = {
                id: state.length + 1,
                name: action.comment.name,
                body: action.comment.body,
                email: action.comment.email,
            }
            state.push(newComment)
            return state
        default:
            return state
    }
}

const select = (state = [], action) => {
    switch (action.type) {
        case 'SELECT':
            state.push(action.comment)
            return state
        case 'DELETE':
            return state.filter((item) => item.id !== action.comment.id)
        default:
            return state
    }
}

export { comments, select }
