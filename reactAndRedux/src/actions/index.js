export const commentsFetched = (comments) => ({
    type: 'FETCH_COMMENTS_SUCCESS',
    comments,
})

export const selectComment = (selectComment) => {
    return {
        type: 'SELECT',
        selectComment,
    }
}

export const addComment = (comment) => {
    a
    return {
        type: 'ADD',
        comment,
    }
}

export const deleteComment = (comment) => {
    return {
        type: 'DELETE',
        comment,
    }
}

