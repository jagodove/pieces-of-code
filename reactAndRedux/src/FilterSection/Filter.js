import React from 'react'
import '../style/sections.css'
import { Route } from 'react-router-dom'
import { openHomepage } from '../Utils/UtilsFunctions'
import SectionForm from './../Components/SectionForm'
import FormButtons from './../Components/FormButtons'
import Error from '../Components/Error'
import Title from '../Components/Title'

class Filter extends React.Component {
    state = {
        letters: '',
        text: '',
        options: null,
        result: null,
        showError: null,
    }
    validate = (opt, e) => {
        let re
        let errorMsg

        opt === 'letters' ? (re = /[a-zA-Z]+/g) : (re = /^[A-Za-z\s]+$/)

        if (!re.test(e.key)) {
            e.preventDefault()
            opt === 'letters'
                ? (errorMsg = 'The name may contain only letters')
                : (errorMsg = 'The comment may contain only letters and space')
            this.setState({showError: errorMsg})
        } else {
            this.setState({showError: null})
        }
    }
    setValue = (opt, value) => {
        this.setState({[opt]: value})
    }
    changeOption = (event) => {
        this.setState({options: event.target.value})
    }
    filter = () => {
        if (
            this.state.options === null ||
            this.state.letters === '' ||
            this.state.text === ''
        ) {
            this.setState({
                showError:
                    'You must fill in the Letters, Text field and select one of the options',
            })
        } else {
            let result = []
            let letters = [...this.state.letters]
            let text = [...this.state.text]

            for (let i = 0; i < text.length; i++) {
                let letter = text[i]
                if (this.state.options === 'delete') {
                    if (
                        !letters.includes(letter.toUpperCase()) &&
                        !letters.includes(letter.toLowerCase())
                    ) {
                        result.push(letter)
                    }
                } else {
                    if (
                        letters.includes(letter.toUpperCase()) ||
                        letters.includes(letter.toLowerCase())
                    ) {
                        result.push(letter)
                    }
                }
            }
            this.setState({
                letters: '',
                text: '',
                result: result.join(''),
                showError: null,
            })
        }
    }
    clearState = () => {
        this.setState(
            {
                letters: '',
                text: '',
                options: null,
                result: null,
                showError: null,
            },
            () => {
                openHomepage()
            },
        )
    }

    render () {
        return (
            <React.Fragment>
                <section className="list" id="thrid">
                    <div className="form-comment">
                        <Route path="/" component={() => <Title
                            title={'Filter'}/>}/>{' '}
                        <SectionForm
                            title={'Letters'}
                            type={'text'}
                            value={this.state.letters}
                            valueName={'letters'}
                            onChange={this.setValue.bind(this)}
                            onKeyPress={this.validate.bind(this)}/>
                        <SectionForm
                            title={'Text'}
                            type={'text'}
                            value={this.state.text}
                            valueName={'text'}
                            onChange={this.setValue.bind(this)}
                            onKeyPress={this.validate.bind(this)}/>
                        {this.state.showError != null && (
                            <Error msg={this.state.showError}/>
                        )}
                        {this.state.result != null && (
                            <div
                                className="result">RESULT: {this.state.result}</div>
                        )}
                        <div className="select">
                            <select id="select-section"
                                    onChange={this.changeOption}>
                                <option value=" ">--Select option--</option>
                                <option value="delete">Delete</option>
                                <option value="leave">Leave</option>
                            </select>
                            <div className="select_arrow"></div>
                        </div>
                        <FormButtons
                            buttonClass="comment-btn"
                            open={this.clearState}
                            submit={this.filter}
                            onChange={this.setValue.bind(this)}
                            onKeyPress={this.validate.bind(this)}
                            title={['Back to homepage', 'Filter']}/>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}

export default Filter

