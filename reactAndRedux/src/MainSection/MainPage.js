import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import PropTypes from 'prop-types'
import '../style/sections.css'
import List from '../Components/List'
import Title from '../Components/Title'

class MainPage extends React.Component {
    render () {
        return (
            <Router>
                <section className="list" id="first">
                    <div className="list">
                        <Route
                            path="/"
                            component={() => (
                                <Title title={'LIST OF COMMENTS'}/>)}/>
                        <Route path="/"
                               component={() => <List
                                   comments={this.props.comments}
                                   addSelectedComments={true}/>}/>
                    </div>
                </section>
            </Router>

        )
    }
}

export default MainPage

MainPage.propTypes = {
    comments: PropTypes.array,
}
