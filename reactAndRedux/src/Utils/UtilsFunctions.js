function openHomepage () {
    window.location.href = '#first'
}

function openSectionSecond () {
    window.location.href = '#second'
}

function refresh () {
    setTimeout(() => document.getElementById('selected').click(), 0)
}

export { openSectionSecond, openHomepage, refresh }
