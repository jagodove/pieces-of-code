var allFrogs = {}
var characteristics = [['tall', 'short'], ['fat', 'slim']]
let selectedFrogs = []
let frogsId = [null, null]
let newPlaces = null

function checkedFrog () {
    if (event.target.checked) {
        if (event.target.labels[0].className === '') {
            newPlaces = parseInt(event.target.id)
        } else {
            if (frogsId[0] === null) {
                frogsId[0] = parseInt(event.target.id)
                selectedFrogs[0] = event.target.labels[0].className.slice(5)
            } else {
                frogsId[1] = parseInt(event.target.id)
                selectedFrogs[1] = event.target.labels[0].className.slice(5)
                if (Object.keys(allFrogs).length === 0) { // przypisanie cech dwóm pierwszym żabom;
                    addCharacteristicsc(characteristics)
                    addCharacteristicsc(characteristics)
                }
            }
        }
    } else {
        let frogToDeleted = frogsId.indexOf(parseInt(event.target.id))
        selectedFrogs[frogToDeleted] = null
        frogsId[frogToDeleted] = null
    }

}

function addCharacteristicsc (options) {
    let allFrogsSize = Object.keys(allFrogs).length
    allFrogs[allFrogsSize] = {
        id: allFrogsSize,
        frogCharacteristicsc:
            [
                options[0][Math.round(Math.random())],
                options[1][Math.round(Math.random())]],
    }
}

function jumpingFrog () {
    document.getElementById('label-' + frogsId[0]).
        classList.
        remove('frog', selectedFrogs[0])
    document.getElementById('label-' + newPlaces).
        classList.
        add('frog', selectedFrogs[0])
    document.getElementById('label-' + frogsId[0]).dataset.number = ''
    document.getElementById('label-' + newPlaces).dataset.number = frogsId[0]
}

function clearData () {
    frogsId[0] !== null
        ? document.getElementById(frogsId[0]).checked = false
        : null
    frogsId[1] !== null
        ? document.getElementById(frogsId[1]).checked = false
        : null
    newPlaces !== null
        ? document.getElementById(newPlaces).checked = false
        : null
    frogsId = [null, null]
    selectedFrogs = [null, null]
    newPlaces = null
}

function jump () {
    if (frogsId[0] !== null && newPlaces !== null) {
        let jumpOption = checkJumpOption()
        if (jumpOption) {
            jumpingFrog()
        } else {
            alert('Your frog can\'t jump in here')
        }

    } else {
        alert('You have to choose a frog and a jumping place')
    }

    clearData()
}

function checkJumpOption () {
    let jumpLength
    let frogId = frogsId[0]
    selectedFrogs[0] === 'female' ? jumpLength = 2 : jumpLength = 3
    if (jumpLength + frogId == newPlaces || //skok w prawo
        frogId - jumpLength == newPlaces || // skok w lewo
        jumpLength * 10 + frogId == newPlaces || //skok w dół
        frogId - (jumpLength * 10) == newPlaces ||  // skok w górę
        jumpLength * 10 + frogId + jumpLength == newPlaces || //po przekątnej w prawo i w dół
        frogId - (10 * jumpLength) - jumpLength == newPlaces || // po przekątnej w lewo i w górę
        frogId - (10 * jumpLength) + jumpLength == newPlaces ||  //po przekątnej w prawo i w górę
        frogId + (10 * jumpLength) - jumpLength == newPlaces) { // po przekątnej w lewo i w dół
        return true
    } else {
        return false
    }
}

function frogCharacterOptions () {
    let firstParent = allFrogs[document.getElementById(
        'label-' + frogsId[0]).dataset.number]
    let secondParent = allFrogs[document.getElementById(
        'label-' + frogsId[1]).dataset.number]
    let options = [
        [
            firstParent.frogCharacteristicsc[0],
            secondParent.frogCharacteristicsc[0]],
        [
            firstParent.frogCharacteristicsc[1],
            secondParent.frogCharacteristicsc[1]],
    ]

    addCharacteristicsc(options)
}

function possibilitiesPlaces (id) {
    var places = []
    if ((id + 1) % 10 !== 1) {
        places.push(id + 1)
    }
    if (id % 10 === 0) {
        places.push(id - 1)
    }
    if (id - 10 >= 0) {
        places.push(id - 10)
    }
    if (id - 9 >= 0) {
        places.push(id - 9)
    }
    if (id - 11 >= 0) {
        places.push(id - 11)
    }
    if (id + 10 <= 60) {
        places.push(id + 10)
    }
    if (id + 9 <= 60) {
        places.push(id + 9)
    }
    if (id + 11 <= 60) {
        places.push(id + 11)
    }

    return places
}

function drawNewFrog (motherId) {
    let randomNumber = Math.round(Math.random())
    let numberNewFrog = Object.keys(allFrogs).length
    let labelId

    let possibilities = possibilitiesPlaces(motherId)

    for (let i = 0; i < possibilities.length; i++) {
        let place = possibilities[i]
        if (document.getElementById('label-' + place).className === '') {
            labelId = place
            document.getElementById('label-' + place).
                classList.
                add('frog', selectedFrogs[randomNumber])
            break
        }
    }

    if (labelId !== undefined) {
        document.getElementById(
            'label-' + labelId).dataset.number = numberNewFrog
    } else {
        alert('No places adjacent to the mother')
    }

}

function reproduce () {
    let errorMsg = 'You must choose two frogs of different sexes in order to multiply '
    if (frogsId[0] !== null && frogsId[1] !== null) {
        if (selectedFrogs[0] !== selectedFrogs[1]) {
            let motherId = frogsId[selectedFrogs.indexOf('female')]
            drawNewFrog(motherId)
            frogCharacterOptions()
        } else {
            alert(errorMsg)
        }
    } else {
        alert(errorMsg)
    }

    clearData()
}