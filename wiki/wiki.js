var result = {}
var Update = {}
Update.requestClassifieds = function (i) {
    return new Promise((resolve, reject) => {
        resolve(i)
    })
}

function start (category) {
    result = {}
    var category = category
    fetch(
        'https://en.wikipedia.org/w/api.php?action=query&list=categorymembers&cmtitle=Category:' +
        category + '&cmlimit=500&format=json&origin=*').
        then(resp => resp.json()).
        then(resp => {
            var pageId = []

            resp.query.categorymembers.forEach(item => {
                var id = item.pageid
                if (item.ns === 0) {
                    pageId.push(item.pageid)
                }
            })
            pageId.shift()
            console.log(pageId)
            var promisesChain = Promise.resolve()

            for (var i in pageId) {
                promisesChain = promisesChain.then(((i) => () => {
                    return Update.requestClassifieds(pageId[i])
                })(i)).then((res) => {
                    return new Promise((resolve) => {
                        setTimeout(() => {
                            var pageIds = res
                            fetch(
                                'https://en.wikipedia.org/w/api.php?action=query&prop=extracts&pageids=' +
                                pageIds + '&format=json&origin=*').
                                then(resp => resp.json()).
                                then(resp => {
                                    console.log(resp.query.pages)
                                    var query = resp.query.pages
                                    var string
                                    for (var prop in query) {
                                        string = stripHtml(query[prop].extract)
                                    }

                                    countFrequency(string)
                                })
                            resolve(updateResult(category))
                        }, 1000)
                    })
                })
            }
        })

}

function countFrequency (string) {
    var check = string.match(/\b(\w)/g).join('')

    check.split('').filter(function (letter) {
        return letter.match(/[a-z]/i)
    }).forEach(function (letter) {
        letter = letter.toLowerCase()
        result[letter] = result[letter] ? result[letter] + 1 : 1
    })

    var bySort = Object.assign({}, result)
    result = Object.keys(bySort).sort().reduce((accumulator, currentValue) => {
        accumulator[currentValue] = bySort[currentValue]
        return accumulator
    }, {})

    return result
}

function stripHtml (html) {
    var tmp = document.createElement('DIV')
    tmp.innerHTML = html
    return tmp.textContent || tmp.innerText || ''
}

function updateResult (category) {
    (function () {
        setTimeout(function () {
            document.getElementById(
                'json-' + category).innerHTML = JSON.stringify(result,
                undefined, 2)
        }, 1000)
    })()
}

