/**
 * Created by jagoda on 05.07.17.
 */
var one = 'ATCTGAT'
var two = 'TGCATA'

//dostajemy dwa różne motywy i chcemy przeplatać je razem w taki sposób, aby uzyskać jak najkrótszy ciąg genetyczny, w którym oba motywy występują jako sekwencje.
var motif = []

for (var i = 0; i < one.length; i++) {

    if (one[i] === two[i]) {
        motif.push(one[i])
    } else if (one[i] !== two[i] && one[i] !== two[i + 1] && one[i] !==
        motif[motif.length - 1] || motif.length === 0) {

        if ((one[i] + two[i]).indexOf('undefined') === -1) {
            motif.push(one[i])
            motif.push(two[i])
        } else {
            motif.push(one[i])
        }
    } else if (one[i] !== two[i] && one[i] !== two[i + 1] && one[i] ===
        motif[motif.length - 1]) {

        motif.push(two[i])
    } else if (one[i] !== two[i] && one[i] === two[i + 1]) {
        motif.push(two[i])
        motif.push(one[i])
    }

}

var Motif = motif.join('')

console.log(Motif)