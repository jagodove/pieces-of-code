/** @module VectorMath */

var VectorMath = {
    /** @method VectorProd
     *  @desc Adds two or four vectors (vectors in three-dimensional space) and returns the vector product(another name is cross product).
     *  @param {Array.<number>} firstVector - the first vector for calculations (required)
     *  @param {Array.<number>} secondVector - the second vector for calculations (required)
     *  @param {Array.<number>} thirdVector - the third vector for calculations (optional)
     *  @param {Array.<number>} fourthVector - the fourth vector for calculations (optional)
     *  @returns {Array.<number>} returns a new vector
     */
    VectorProd: function VectorProd (
        firstVector,
        secondVector,
        thirdVector,
        fourthVector,
    ) {
        thirdVector = thirdVector || 0
        fourthVector = fourthVector || [0, 0, 0]

        var vector_prod

        if (thirdVector === 0) {
            vector_prod = [
                firstVector[1] * secondVector[2] - firstVector[2] *
                secondVector[1],
                firstVector[2] * secondVector[0] - firstVector[0] *
                secondVector[2],
                firstVector[0] * secondVector[1] - firstVector[1] *
                secondVector[0],
            ]
        } else if (thirdVector !== 0) {
            vector_prod = [
                (firstVector[1] - secondVector[1]) *
                (thirdVector[2] - fourthVector[2]) -
                (firstVector[2] - secondVector[2]) *
                (thirdVector[1] - fourthVector[1]),
                (firstVector[2] - secondVector[2]) *
                (thirdVector[0] - fourthVector[0]) -
                (firstVector[0] - secondVector[0]) *
                (thirdVector[2] - fourthVector[2]),
                (firstVector[0] - secondVector[0]) *
                (thirdVector[1] - fourthVector[1]) -
                (firstVector[1] - secondVector[1]) *
                (thirdVector[0] - fourthVector[0]),
            ]
        }

        return vector_prod
    },
    /** @method ScalarProd
     * @desc Adds two or four vectors and returns the scalar product (another name is dot product).
     * @param {Array.<number>} firstVector - the first vector for calculations (required).
     * @param {Array.<number>} secondVector - he second vector for calculations (required).
     * @param {Array.<number>} thirdVector - the third vector for calculations (optional).
     * @param {Array.<number>} fourthVector - the fourth vector for calculations (optional).
     * @returns {number} returns a scalar quantity (a number with units).
     */
    ScalarProd: function ScalarProd (
        firstVector,
        secondVector,
        thirdVector,
        fourthVector,
    ) {
        thirdVector = thirdVector || 0
        fourthVector = fourthVector || [0, 0, 0]

        var scalar_prod
        if (thirdVector === 0) {
            scalar_prod =
                firstVector[0] * secondVector[0] +
                firstVector[1] * secondVector[1] +
                firstVector[2] * secondVector[2]
        } else if (thirdVector !== 0) {
            var newA = [
                firstVector[0] - secondVector[0],
                firstVector[1] - secondVector[1],
                firstVector[2] - secondVector[2],
            ]
            var newB = [
                thirdVector[0] - fourthVector[0],
                thirdVector[1] - fourthVector[1],
                thirdVector[2] - fourthVector[2],
            ]

            scalar_prod = newA[0] * newB[0] + newA[1] * newB[1] + newA[2] *
                newB[2]
        }

        return scalar_prod
    },
    /** @method Norm
     * @desc Calculate the norm of a number, vector or matrix. Norm means the length of the vector.
     * @param {Array.<number>} firstVector - the first vector for calculations (required)
     * @param {Array.<number>} secondVector - the second vector for calculations (optional)
     * @returns {number} returns the norm of vector
     */
    Norm: function Norm (firstVector, secondVector) {
        secondVector = secondVector || [0, 0, 0]

        var normSqr = 0

        for (var i = 0; i < firstVector.length; i++) {
            normSqr += Math.pow(firstVector[i] - secondVector[i], 2)
        }

        return Math.sqrt(normSqr)
    },
    /** @method substraction
     * @desc  Subtracts the two given vectors
     * @param {Array.<number>} firstVector - the first vector for substract
     * @param {Array.<number>} secondVector - the second vector for substract
     * @returns {Array.<number>} returns a new vector
     */
    subtraction: function subtraction (firstVector, secondVector) {
        var newVector = []
        for (var i = 0; i < firstVector.length; i++) {
            newVector[i] = firstVector[i] - secondVector[i]
        }

        return newVector
    },
    /** @method addVectors
     * @desc Adds two given vectors.
     * @param {Array.<number>} firstVector - the first vector for add.
     * @param {Array.<number>} secondVector - the second vector for add.
     * @returns {Array.<number>} returns new vector.
     */
    addVectors: function addVectors (firstVector, secondVector) {
        var newVector = []
        for (var i = 0; i < firstVector.length; i++) {
            newVector[i] = firstVector[i] + secondVector[i]
        }
        return newVector
    },
    /** @method multiplyVector
     *  @desc Multiplication of the vector by the given number(multiplier).
     * @param {number} scalar - number to multiply the vector.
     * @param {Array.<number>} vector - vector for multiplication.
     * @returns {Array.<number>} returns new vector.
     */
    multiplyVector: function multiplyVector (scalar, vector) {
        var newVector = []
        for (var i = 0; i < vector.length; i++) {
            newVector[i] = scalar * vector[i]
        }
        return newVector
    },
    /**@method divisionVector
     * @desc Obtaining a new vector by dividing the values of a given vector and divisor
     * @param {number} divider - value of the divisor
     * @param {Array.<number>} vector - vector to divide
     * @returns {Array.<number>} returns new vector
     */
    divisionVector: function divisionVector (divider, vector) {
        var newVector = []
        for (var i = 0; i < vector.length; i++) {
            newVector[i] = vector[i] / divider
        }
        return newVector
    },
    /** @method det
     * @desc  Calculates the determinant of a pair of two-dimensional vectors
     * @param {Array.<number>} firstVector - first vector
     * @param {Array.<number>} secondVector -second vector
     * @returns {number} returns determinant of a pair of two-dimensional vectors
     */
    det: function det (firstVector, secondVector) {
        return firstVector[0] * secondVector[1] - firstVector[1] *
            secondVector[0]
    },
}

export { VectorMath }
